from flask import Flask, request, redirect, url_for
import os, json
import requests
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

access_token=''

@app.route('/')
def hello_world():
   return 'Hello World'

@app.route('/exchange')
def get_access_token():
   if request.method == 'GET':
      code = request.args.get('code')

      # client_id `43d7d72f-fda9-4600-bb7f-ea52527c64cc`
      # client_secret `e610ece2-0311-4032-9dc3-c14a7ad773f1`
      # Base64 encode({client_id}:{client_secret})'

      headers = {
         'Authorization': 'Basic NDNkN2Q3MmYtZmRhOS00NjAwLWJiN2YtZWE1MjUyN2M2NGNjOmU2MTBlY2UyLTAzMTEtNDAzMi05ZGMzLWMxNGE3YWQ3NzNmMQ==',
      }
      data = {
         'code': code,
         'redirect_uri': 'https://tryfinch.com'
      }

      response = requests.post('https://api.tryfinch.com/auth/token', headers=headers, data=data)

      global access_token 
      access_token = response.json()['access_token']
      return access_token

@app.route('/organization')
def get_organization():
   global access_token

   headers = {
    'Authorization': 'Bearer '+access_token,
    'Finch-API-Version': '2020-09-17',
   }
   response = requests.get('https://api.tryfinch.com/employer/company', headers=headers)
   return response.json()

if __name__ == '__main__':
   app.run()
